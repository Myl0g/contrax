import 'dart:typed_data';
import 'dart:convert';

import 'package:http/http.dart' as http;

class API {
  static final prefix = "http://localhost:8080";
  final String username;
  String salt;
  String token;

  API(this.username);

  static Future<List<String>> getUsers() async {
    final jsonRes = (await http.get(prefix + "/users")).body;

    if (jsonRes == "null") {
      return [];
    }

    return List<String>.from(json.decode(jsonRes));
  }

  Future<Uint8List> downloadPDF(String pdfName) async {
    return (await http.get(
            prefix + "/users/" + this.username + "/files/" + pdfName,
            headers: {"Authorization": this.username + ":" + this.token}))
        .bodyBytes;
  }

  Future<bool> uploadPDF(Uint8List bytes, String pdfName,
      [String recipient = ""]) async {
    if (recipient == "") {
      recipient = this.username;
    }
    final response = await http.post(
        prefix + "/users/" + recipient + "/files/" + pdfName,
        body: bytes,
        headers: {"Authorization": this.username + ":" + this.token});
    return response.statusCode == 200;
  }

  Future<void> updateSaltFromServer() async {
    this.salt =
        (await http.get(prefix + "/users/" + this.username + "/account/salt"))
            .body;
  }

  Future<List<String>> getIncomingFileNames() async {
    final jsonRes = (await http.get(
            prefix + "/users/" + this.username + "/filenames/incoming",
            headers: {"Authorization": this.username + ":" + this.token}))
        .body;

    if (jsonRes == "null") {
      return [];
    }

    final rawNames = List<String>.from(json.decode(jsonRes));
    return rawNames.map((s) => s.replaceFirst("/", "")).toList();
  }

  Future<List<String>> getOutgoingFileNames() async {
    final jsonRes = (await http.get(
            prefix + "/users/" + this.username + "/filenames/outgoing",
            headers: {"Authorization": this.username + ":" + this.token}))
        .body;

    if (jsonRes == "null") {
      return [];
    }

    final rawNames = List<String>.from(json.decode(jsonRes));
    return rawNames.map((s) => s.replaceFirst("/", "")).toList();
  }

  Future<bool> authenticate(String hash, String passcode) async {
    final resp = await http
        .get(prefix + "/users/" + this.username + "/account", headers: {
      "Authentication": this.username +
          ":" +
          base64.encode(utf8.encode(this.salt)) +
          ":" +
          base64.encode(utf8.encode(hash)) +
          ":" +
          base64.encode(utf8.encode(passcode))
    });

    if (resp.statusCode == 200) {
      this.token = resp.body;
      return true;
    } else {
      return false;
    }
  }

  Future<Uint8List> registerTotp(String hash) async {
    final resp = await http
        .get(prefix + "/users/" + this.username + "/account", headers: {
      "Authentication": this.username +
          ":" +
          base64.encode(utf8.encode(this.salt)) +
          ":" +
          base64.encode(utf8.encode(hash)) +
          ":" +
          base64.encode(utf8.encode("000000"))
    });

    if (resp.statusCode == 202) {
      return resp.bodyBytes;
    } else {
      return null;
    }
  }

  Future<bool> isAdmin() async {
    final resp = (await http.get(
        prefix + "/users/" + this.username + "/account/admin",
        headers: {"Authorization": this.username + ":" + this.token}));

    return resp.statusCode == 200;
  }
}
