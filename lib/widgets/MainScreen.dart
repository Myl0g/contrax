import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

import '../api.dart';
import 'Document.dart';
import 'UploadAsAdmin.dart';

class MainScreen extends StatelessWidget {
  final API api;
  final List<String> incomingFiles;
  final List<String> outgoingFiles;

  MainScreen(
      {Key key,
      @required this.api,
      @required this.incomingFiles,
      @required this.outgoingFiles});

  Future<void> _uploadFile(BuildContext context) async {
    html.InputElement uploadInput = html.FileUploadInputElement();
    uploadInput.multiple = false;
    uploadInput.draggable = true;
    uploadInput.accept = ".pdf";
    uploadInput.click();

    uploadInput.onChange.listen((event) {
      final file = uploadInput.files[0];
      final reader = html.FileReader();
      reader.onLoadEnd.listen((e) async {
        final uploadStatus = await (api.uploadPDF(
          reader.result,
          file.name,
        ));

        if (uploadStatus) {
          showDialog(
              context: context,
              child: AlertDialog(
                title: const Text("Success"),
                content: const SingleChildScrollView(
                  child: const Text(
                      "Your file uploaded successfully! Please note that your files may not be visible unless you refresh the page."),
                ),
                actions: [
                  FlatButton(
                    child: const Text("OK"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
        } else {}
      });

      reader.readAsArrayBuffer(file);
    });
  }

  Future<void> _uploadAdminFile(BuildContext context) async {
    showDialog(
      context: context,
      child: UploadAsAdmin(api: this.api),
    );
  }

  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              const Tab(
                text: "Documents for You",
              ),
              const Tab(
                text: "Documents You've Sent",
              )
            ],
          ),
          title: Text("Welcome to contrax, " + this.api.username + "."),
        ),
        body: TabBarView(
          children: [
            DocumentPage(
              api: api,
              fileNames: incomingFiles,
            ),
            DocumentPage(
              api: api,
              fileNames: outgoingFiles,
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            if (!(await api.isAdmin())) {
              await _uploadFile(context);
            } else {
              await _uploadAdminFile(context);
            }
          },
          child: Icon(Icons.cloud_upload),
        ),
      ),
    );
  }
}
