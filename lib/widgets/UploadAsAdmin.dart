import 'package:contrax/api.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

class UploadAsAdmin extends StatelessWidget {
  final API api;
  final TextEditingController _intendedUserController = TextEditingController();

  UploadAsAdmin({@required this.api});

  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 300,
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  "Send a File",
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(6.0),
                child: const Text(
                  "To send a file, enter in a username (or enter * to send to all users) and upload your file.",
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: TextField(
                    controller: _intendedUserController,
                    enableSuggestions: false,
                    autocorrect: false,
                    decoration: InputDecoration(
                      labelText: "Enter a username",
                      border: OutlineInputBorder(),
                    ),
                  )),
              FlatButton.icon(
                label: const Text("Upload File"),
                icon: const Icon(Icons.file_upload),
                onPressed: () async {
                  html.InputElement uploadInput = html.FileUploadInputElement();
                  uploadInput.multiple = false;
                  uploadInput.draggable = true;
                  uploadInput.accept = ".pdf";
                  uploadInput.click();

                  uploadInput.onChange.listen((event) {
                    final file = uploadInput.files[0];
                    final reader = html.FileReader();
                    reader.onLoadEnd.listen((e) async {
                      final uploadStatus = await (api.uploadPDF(
                        reader.result,
                        file.name,
                        _intendedUserController.text,
                      ));

                      if (uploadStatus) {
                        showDialog(
                            context: context,
                            child: AlertDialog(
                              title: const Text("Success"),
                              content: const SingleChildScrollView(
                                child: const Text(
                                  "Your file uploaded successfully! Reload to see it in your Documents You've Sent tab.",
                                ),
                              ),
                              actions: [
                                FlatButton(
                                  child: const Text("OK"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            ));
                      } else {
                        showDialog(
                            context: context,
                            child: AlertDialog(
                              title: const Text("Failure"),
                              content: const SingleChildScrollView(
                                child: const Text(
                                  "Your file could not be uploaded. Check your internet connection or contact your system administrator.",
                                ),
                              ),
                              actions: [
                                FlatButton(
                                  child: const Text("OK"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            ));
                      }
                    });

                    reader.readAsArrayBuffer(file);
                  });
                },
              ),
              FlatButton.icon(
                icon: const Icon(
                  Icons.close,
                ),
                label: const Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
