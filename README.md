# contrax

A secure document exchange system.

## Purpose

I designed this system when I began offically freelancing as a developer, in order to facilitate the secure and organized exchange of contracts between me and my clients (i.e. NDAs, rate agreements, etc.).

## Implementation

The system consists of two components: the user-facing website, and the [backend API](https://gitlab.com/myl0g/contrax_backend) which coordinates requests and securely stores user data.

This repository contains the front-end (user-facing) component, and its implementation is described below:

1. Install [flutter](https://flutter.dev/).
2. Switch to the beta channel using `flutter channel beta`.
3. Run `flutter upgrade` to actually implement the switch to beta.
4. Explicitly enable web support by running `flutter config --enable-web`.
5. Clone this repo and change your working directory to it.
6. To test the software, run `flutter run`.
7. Change the value of the variable `static final prefix` in `lib/api.dart` to match the location to which you've deployed the backend. Example: `"https://example.com"`.
8. To create a release build of the software, run `flutter build web`.

## License

See [LICENSE](./LICENSE).
